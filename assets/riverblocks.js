var Riverblocks = Riverblocks || {};

var Riverblocks = {
  initialize: function() {
    categoryList      = document.getElementById('category-list');
    feedPane          = document.getElementById('feeds');
    importForm        = document.getElementById("submit_opml");

    Riverblocks.renderCategories(Riverblocks.getFeeds());
  },
  importOpml: function(){

    var opmlFile = this.files[0];                           // file being processed
    var reader = new FileReader();

    reader.onload = function(){

      let parser    = new DOMParser();
      let doc       = parser.parseFromString(this.result, 'text/xml');
      let opml      = doc.getElementsByTagName("body");     // HTMLCollection

      let feeds     = Riverblocks.convertOpml(opml);        // Object
      Riverblocks.saveFeeds(Riverblocks.convertOpml(opml));

      feedsToRender = Riverblocks.getFeeds();               // Object
      Riverblocks.renderCategories(feedsToRender);          // always render from storage
    }
    reader.onerror = function(){ fileOutput.innerText = "Could not read file"; }
    reader.readAsText(opmlFile);                            // read the file

  },
  convertOpml: function(nodes){
    // Returns: Object
    opmlJson = {};

    for(let i=0; i < nodes.length; i++){

      let currentNode = nodes[i];
      let title       = currentNode.getAttribute('title');
      let feedUrl     = currentNode.getAttribute('xmlUrl');
      let feedType    = currentNode.getAttribute('type');

      if (!feedType && !feedUrl) {
        if(!title) {
          // Some top-level tag like 'body'; just send it through again.
          // TODO: learn how to chop the BODY tag so I don't have
          // to do this.
          if(currentNode.children.length > 0) {
            Riverblocks.convertOpml(currentNode.children);
          }
        } else {
          // This is a category

          // create the category even if it winds up being empty
          opmlJson[title] = [];
          
          if(currentNode.children.length > 0) {
            // add them to the collection
            opmlJson[title] = Riverblocks.categoryArray(currentNode.children);
          }
        }
      }
    }
    return opmlJson;
  },
  categoryArray: function(nodes) {
    // Returns: Object
    feedArray = [];
    for(var i = 0; i < nodes.length; i++) {
      let node  = nodes[i];

      nodeProps = {
         title:   node.getAttribute('title'),
         feedUrl: node.getAttribute('xmlUrl'),
         siteUrl: node.getAttribute('htmlUrl')
      }

      feedArray.push(nodeProps);
    }
    return feedArray;
  },
  renderCategories: function(nodes) {
    // Parameters: nodes(Object)
    // Categories rendered in HTML as an accordion.
    Object.entries(nodes).forEach(function(entry, idx) {
      let categoryName      =  entry.shift();
      let controlName       = `category_${idx+1}`;
      let ul                = document.createElement('UL');
      let category          = document.createElement('LI');
      category.className   += 'category';

      let settingsIcon      = document.createTextNode("\u24D8");
      let actions           = document.createElement('SPAN');
      actions.className    += 'settings';
      actions.title         = 'Settings';
      actions.appendChild(settingsIcon);

      let label             = document.createElement('LABEL');
      label.htmlFor         = controlName;
      label.innerText       = categoryName;
      label.appendChild(actions);

      let checkbox          = document.createElement('INPUT');
      checkbox.type = 'checkbox';
      checkbox.name = controlName;
      checkbox.id   = controlName;

      // Put the category heading chrome together
      category.appendChild(checkbox);
      category.appendChild(label);

      // build a UL with the category contents
      categoryFeeds = Riverblocks.populateCategoryHtml(entry.shift());
      // append them to this category's LI
      category.appendChild(categoryFeeds);
      // add the category to the parent
      categoryList.appendChild(category);
    });
  },
  populateCategoryHtml: function(feedNodes){
    // per-site category LI tags
    // Returns: UL DOM element
    list = document.createElement('UL');
    list.className += 'feeds'

    for(var i = 0; i < feedNodes.length; i++) {

      let currentNode         = feedNodes[i];
      let categoryTitle       = document.createTextNode(currentNode['title']);
      let li                  = document.createElement("LI");
      let categoryLink        = document.createElement('A');

      categoryLink.className += 'feed';

      // Add some helpful stuff to the link
      categoryLink.setAttribute('href', "#");
      categoryLink.setAttribute('data-feed-url', currentNode.feedUrl);
      categoryLink.setAttribute('data-site-url', currentNode.siteUrl);

      // wrap the title with the link
      categoryLink.appendChild(categoryTitle);

      // attach the click handler event
      if (window.attachEvent){
        categoryLink.attachEvent('click', Riverblocks.retrieveFeed);
      } else {
        categoryLink.addEventListener('click', Riverblocks.retrieveFeed);
      }

      // put the LI in the UL
      list.appendChild(li);

      // wrap the link with an LI
      list.lastChild.appendChild(categoryLink);
    }
    return list;
  },
  saveFeeds: function(feeds){
    localStorage.setItem('feeds', JSON.stringify(feeds));
  },
  getFeeds: function(){
    // Returns: Object
    if (localStorage.getItem('feeds') === null) {
      return false;
    } else {
      return JSON.parse(localStorage.getItem('feeds'));
    }
  },
  updateFeeds: function(){
    // change settings or add new feeds
  },
  retrieveFeed: function(){
    // when a feed is clicked, list stories in feeds pane
    Riverblocks.setActiveClass(this);
    let feedPane          = document.getElementById('feeds');
    let spinner           = feedPane.getElementsByClassName('spinner');
    spinner[0].classList.add('loading');

    fetch('https://cors-anywhere.herokuapp.com/'+this.dataset.feedUrl)
      .then(response  => response.text())
      .then(str       => (new window.DOMParser()).parseFromString(str,'text/xml'))
      .then(data      => Riverblocks.parseFeed(data)); 
  },
  setActiveClass(child){
     currentActive = document.getElementsByClassName('active')
     newActive = child.parentElement
     for(let i = 0; i < currentActive.length; i++){
        currentActive[i].classList.remove('active');
     }
     newActive.classList.add('active')
  },
  cullFeed: function() {
    console.log("cullFeed(e)", e);
  },
  parseFeed: function(feed) {
    // Takes the raw feed XML, grab whatever elements I need,
    // puts them in an array, and passes the array along to
    // RB#renderFeed

    // per-site
    let rss               = feed.getElementsByTagName('rss')[0];
    if(rss !== null){

      let feedItems       = rss.getElementsByTagName('item');
      let parsedStories   = [];
      // let siteInfo      = feed.getElementsByTagName('channel');

      // loop through current items in this feed and
      // create an array of items with the needed info
      for(let i = 0; i < feedItems.length; i++){
        let item = feedItems[i];
        parsedStories.push({ "title" : item.getElementsByTagName('title')[0].textContent, "link" : item.getElementsByTagName('link')[0].textContent });
      }

      // Display the feed's list of items
      Riverblocks.renderFeed(parsedStories);

    } else {
      console.log("parseFeed(rss) error:", rss);
    }
  },
  renderFeed: function(data){
    // data: <item>

    let feedsList       = document.getElementById('feeds');
    let storyList       = document.createElement('UL');

    for(let i = 0; i < data.length; i++){
      let title         = data[i].title
      let storyTitle    = document.createTextNode(title);
      let li            = document.createElement("LI");
      let storyLink     = document.createElement('A');
      storyLink.href    = data[i].link;
      storyLink.title   = title;
      storyLink.target  = "_blank";
      storyLink.rel     = "noopener";

      storyLink.appendChild(storyTitle);
      if (window.attachEvent){
        storyLink.attachEvent('click', Riverblocks.retrieveStory);
      } else {
        storyLink.addEventListener('click', Riverblocks.retrieveStory);
      }

      li.appendChild(storyLink);
      storyList.appendChild(li);
    }

    // turn off loading.gif
    let spinner = feedPane.getElementsByClassName('loading');
    spinner[0].classList.remove('loading');

    // remove and/or replace the current contents of the feeds pane
    oldList = feedsList.getElementsByTagName('UL');
    if(oldList.length > 0){
      // previous storyList exists, so delete it
      feedsList.removeChild(oldList[0]);
    }
    // tack the new one on
    feedsList.appendChild(storyList);
  },
  retrieveStory: function(e){
    e.preventDefault();
    // console.log("renderStory(e.target.href):", e.target.href);
    fetch('https://cors-anywhere.herokuapp.com/'+e.target.href)
      .then(response  => response.text())
      .then(data      => Riverblocks.renderStory(data)); 
  },
  renderStory: function(e){
    let storyPane   = document.getElementById('story');
    console.log("retrieveStory(e):", e);
    let siteContent = e.getElementsByTagName('article');
    // let siteContent = e;
    content = document.createTextNode(siteContent);
    storyPane.appendChild(content);
  },
  extractStoryText: function(){
    console.log("extractStoryText(this):", this);
  }
}