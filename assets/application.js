var Riverblocks = Riverblocks || {};

window.onload = function() {

  setupPage();

  fileInput     = document.getElementById('opml_file');
  fileInput.addEventListener('change', Riverblocks.importOpml);

  fileOutput  = document.getElementById('display');

}

function setupPage() {
  checkStorage();
  Riverblocks.initialize();
}

function checkStorage() {
  if(typeof Storage !== "undefined") {
    // debugMessage("OK: Storage (length: "+Storage.length+")");
    if(typeof localStorage !== "undefined") {
      // debugMessage("OK: localStorage");
    }
    if (window.File && window.FileReader && window.FileList && window.Blob) {
      // debugMessage("OK: File APIs");
    } else {
      debugMessage('FAIL: File APIs not found');
    }
  } else {
    debugMessage("FAIL: Storage");
    getElementById("container").style = "display: none";
  }
}
function flashMessage(text) {
  container = document.getElementById("container");
  document.body.insertBefore(nodifyMessage(text), container);
}
function debugMessage(text) {
  console.log(text);
  // container = document.getElementById("debug");
  // container.appendChild(nodifyMessage(text));
}
function nodifyMessage(text) {
  var node = document.createElement("p");
  var message = document.createTextNode(text);
  node.appendChild(message);
  return node;
}
function saveJSON(key,json) {
  localStorage.setItem(key, json);
}
function parseJSON(blob) {
  saveJSON('feeds', JSON.stringify(blob));
}

function handleSubmit(e) {
  console.log("Handling button click!");
  if (e.preventDefault) e.preventDefault();
  var file_field = e.target.parentNode.getElementsByTagName('input');
  var file = file_field.opml_file.files[0];
  return false;
}
